# Databricks notebook source
########################################################################################################################
####################################### Data Design And Modelling Project Part 2 #######################################
#######################################                   Spark                  #######################################
########################################################################################################################
#######################################                  Authors                 #######################################
#######################################              Susanna Ardigò              #######################################
#######################################                Bojken Sina               #######################################
#######################################            Shalini Pandgurang            #######################################
########################################################################################################################
#######################################                   Date                   #######################################
#######################################                19.12.2019                #######################################
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC import spark.implicits._
# MAGIC import org.apache.spark.sql.functions._
# MAGIC import org.apache.spark.sql.Row
# MAGIC import org.apache.spark.sql.types._

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC /**
# MAGIC  * constants
# MAGIC  */
# MAGIC val fileLocation = "/FileStore/tables/Trump_tweets.json";
# MAGIC val fileLocationType = "json";
# MAGIC val delimiter = ",";
# MAGIC /**
# MAGIC  * Reading the json file 
# MAGIC  */
# MAGIC //val dataFrame = spark.read.format(fileLocationType).option("multiline","true").option("mode","FAILFAST").load(fileLocation);
# MAGIC val dataFrame = spark.read.json(fileLocation)
# MAGIC display(dataFrame)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC dataFrame.printSchema()

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val formatFile = "parquet"
# MAGIC val modeFile = "overwrite"
# MAGIC val nameOfParquetFile = "TrumpJsonParquet.parquet"
# MAGIC dataFrame.write.format(formatFile).mode(modeFile).save(nameOfParquetFile)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC display(dbutils.fs.ls("/TrumpJsonParquet.parquet"))

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val parquetData = spark.read.parquet("TrumpJsonParquet.parquet")
# MAGIC display(parquetData)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC parquetData.printSchema()

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val rddData = spark.sparkContext.parallelize(fileLocation);

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
################################# Note First Part Regarding Importing Data Performance #################################
#################################               JSON File: 1.05 seconds                #################################
#################################              Parquet File: 1.02 seconds              #################################
#################################     Creating RDDdata from a JSON file: 0.04 sec      #################################
########################################################################################################################

# In conclusion parquet files are read a little bit slightly than JSON files. As such we conclude that parquet files
# are faster when being imported than JSON files. The very small difference could be due to the amount of the dataset.
# For larger datasets this difference could be higher

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
####################### In the section we are going to check for queries for the following cases #######################
######################            Qeries using Spark considering first the data frame            #######################
######################                  Queries using Spark considering RDD                      #######################
######################                  Queries using Spark considering Paqrquet                 #######################
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
##################################                       QUERY 1                      ##################################
##################################                    Description                     ##################################
################################## Find 10 most followed users ordered alphabetically ##################################
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val top10MostUserFrame = dataFrame
# MAGIC .sort($"user.followers_count".desc)
# MAGIC .select("user.contributors_enabled", "user.created_at", "user.description", "user.follow_request_sent", "user.followers_count", "user.following", "user.friends_count", "user.geo_enabled", "user.id", "user.id_str", "user.lang", "user.listed_count", "user.location", "user.name", "user.notifications", "user.protected", "user.screen_name", "user.statuses_count", "user.time_zone", "user.verified")
# MAGIC .limit(10)
# MAGIC 
# MAGIC display(top10MostUserFrame)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val top10MostUserParquet = parquetData
# MAGIC .sort($"user.followers_count".desc)
# MAGIC .select("user.contributors_enabled", "user.created_at", "user.description", "user.follow_request_sent", "user.followers_count", "user.following", "user.friends_count", "user.geo_enabled", "user.id", "user.id_str", "user.lang", "user.listed_count", "user.location", "user.name", "user.notifications", "user.protected", "user.screen_name", "user.statuses_count", "user.time_zone", "user.verified")
# MAGIC .limit(10)
# MAGIC 
# MAGIC display(top10MostUserParquet)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
#############                                            QUERY 2                                            ############
#############                                         Description                                         ##############
############# Top 15 users with most statusesCount ordered alphabetically and not having language english ##############
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# %scala
# val top10MostStatusCountUserFrame = dataFrame
# .sort($"user.statuses_count".desc, $"user.name".desc)
# .select("user.contributors_enabled", "user.created_at", "user.description", "user.follow_request_sent", "user.followers_count", "user.following", "user.friends_count", "user.geo_enabled", "user.id", "user.id_str", "user.lang", "user.listed_count", "user.location", "user.name", "user.notifications", "user.protected", "user.screen_name", "user.statuses_count", "user.time_zone", "user.verified")
# .filter($"user.lang" !== "en")
# .head(15)
# display(top10MostStatusCountUserFrame)


## ------------------------------------------------------ COMMAND ------------------------------------------------------

# %scala
# val top10MostStatusCountUserParquet = parquetData
# .sort($"user.statuses_count".desc, $"user.name".desc)
# .select("user.contributors_enabled", "user.created_at", "user.description", "user.follow_request_sent", "user.followers_count", "user.following", "user.friends_count", "user.geo_enabled", "user.id", "user.id_str", "user.lang", "user.listed_count", "user.location", "user.name", "user.notifications", "user.protected", "user.screen_name", "user.statuses_count", "user.time_zone", "user.verified")
# .filter($"user.lang" !== "en")
# .head(15)
# display(top10MostStatusCountUserParquet)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
#############################                            QUERY 3                           #############################
#############################                         Description                          #############################
############################# Find 10 most tweets being retweeted and having language "en" #############################
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val topTweetsBeingReTweeted = dataFrame.filter(dataFrame("lang") === "en" && $"retweet_count" > 0)
# MAGIC .sort($"retweet_count".desc)
# MAGIC .limit(10);
# MAGIC display(topTweetsBeingReTweeted)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala 
# MAGIC val topTweetsBeingRetweetedParquet = parquetData.filter(parquetData("lang") === "en" && $"retweet_count" > 0).sort($"retweet_count".desc).head(10);
# MAGIC display(topTweetsBeingRetweetedParquet)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
#########m                                               QUERY 4                                              ##########
#########                                             Description                                             ##########
######### Tweets that contain government in the text, and that have filterLevel low and sort by favoriteCount ##########
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val pattern = ".*Government.*"
# MAGIC val dataFrameText = dataFrame.select($"filter_level")
# MAGIC val dataFrameFilterAndRegex = dataFrame
# MAGIC .filter($"filter_level" === "low")
# MAGIC .filter($"text" rlike pattern)
# MAGIC .sort($"favorite_count".desc)
# MAGIC .select($"text",$"filter_level",$"favorite_count")
# MAGIC .show(10,false)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val pattern = ".*Government.*"
# MAGIC val dataFrameText = parquetData.select($"filter_level")
# MAGIC val dataFrameFilterAndRegex = parquetData
# MAGIC .filter($"filter_level" === "low")
# MAGIC .filter($"text" rlike pattern)
# MAGIC .sort($"favorite_count".desc)
# MAGIC .select($"text",$"filter_level",$"favorite_count")
# MAGIC .show(10,false)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
#############################                            QUERY 5                            ############################
#############################                         Description                           ############################
############################# Count all tweets made in 2018 and not having filter level low ############################
########################################################################################################################


# %scala
# val datePattern = ".*2018.*"
# val dataFrameText = dataFrame.select($"filter_level")
# val dataFrameFilterAndRegex = dataFrame
# .filter($"filter_level" =!= "low")
# .filter($"created_at" rlike datePattern)
# .sort($"favorite_count".desc)
# .select($"text",$"filter_level",$"favorite_count")
# .show(10,false)


## ------------------------------------------------------ COMMAND ------------------------------------------------------


# %scala
# val datePattern = ".*2018.*"
# val dataFrameText = parquetData.select($"filter_level")
# val dataFrameFilterAndRegex = dataFrame
# .filter($"filter_level" =!= "low")
# .filter($"created_at" rlike datePattern)
# .sort($"favorite_count".desc)
# .select($"text",$"filter_level",$"favorite_count")
# .show(10,false)


## ------------------------------------------------------ COMMAND ------------------------------------------------------


########################################################################################################################
############################################             QUERY 6             ###########################################
############################################          Description            ###########################################
############################################ Count tweets hashtaged to trump ###########################################
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val patternTrump = ".*trump.*";
# MAGIC val dataFramePresidentsTrump = dataFrame
# MAGIC .filter(size($"entities.hashtags") > 0) 
# MAGIC .select(explode($"entities.hashtags") as "column")
# MAGIC .withColumn("text", regexp_replace(lower(col("column.text")),patternTrump,"trump"))
# MAGIC .groupBy($"text")
# MAGIC .count()
# MAGIC .filter($"text" === "trump")
# MAGIC display(dataFramePresidentsTrump)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val dataParquePresidentsTrump = parquetData
# MAGIC .filter(size($"entities.hashtags") > 0) 
# MAGIC .select(explode($"entities.hashtags") as "column")
# MAGIC .withColumn("text", regexp_replace(lower(col("column.text")),patternTrump,"trump"))
# MAGIC .groupBy($"text")
# MAGIC .count()
# MAGIC .filter($"text" === "trump")
# MAGIC display(dataParquePresidentsTrump)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
#############                                            QUERY 7                                           #############
#############                                         Description                                          #############
############# Count Hashtags related to puttin and join the data frame with the dataframe calculated above #############
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val patternPuttin = ".*putin.*";
# MAGIC val dataFramePresidentsPuttin = dataFrame
# MAGIC .filter(size($"entities.hashtags") > 0) 
# MAGIC .select(explode($"entities.hashtags") as "column")
# MAGIC .withColumn("text", regexp_replace(lower(col("column.text")),patternPuttin,"puttin"))
# MAGIC .groupBy($"text")
# MAGIC .count()
# MAGIC .filter($"text" === "puttin")
# MAGIC 
# MAGIC val updatedDataFramePresidentsPuttin = dataFramePresidentsPuttin.withColumn("index",lit("1"))
# MAGIC val updatedDataFramePresidentsTrump = dataFramePresidentsTrump.withColumn("index",lit("1"))
# MAGIC 
# MAGIC val joinedDataFrame = updatedDataFramePresidentsTrump.as("trump")
# MAGIC .join(updatedDataFramePresidentsPuttin.as("puttin"),col("trump.index") === col("puttin.index"),"inner")
# MAGIC 
# MAGIC display(joinedDataFrame)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val dataParquePresidentsPuttin = parquetData
# MAGIC .filter(size($"entities.hashtags") > 0) 
# MAGIC .select(explode($"entities.hashtags") as "column")
# MAGIC .withColumn("text", regexp_replace(lower(col("column.text")),patternPuttin,"puttin"))
# MAGIC .groupBy($"text")
# MAGIC .count()
# MAGIC .filter($"text" === "puttin")
# MAGIC 
# MAGIC val updatedDataParquePresidentsPuttin = dataParquePresidentsPuttin.withColumn("index",lit("1"))
# MAGIC val updatedDataParquePresidentsTrump = dataParquePresidentsTrump.withColumn("index",lit("1"))
# MAGIC 
# MAGIC val joinedDataParque = updatedDataParquePresidentsTrump.as("trump")
# MAGIC .join(updatedDataParquePresidentsPuttin.as("puttin"),col("trump.index") === col("puttin.index"),"inner")
# MAGIC display(joinedDataParque)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
###################                                      QUERY 8                                      ##################
###################                                    Description                                    ##################
################### Create a new column in the reuslt in the above dataframe like an id and join them ##################
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val mergedDataFrame = dataFramePresidentsPuttin
# MAGIC .union(dataFramePresidentsTrump.select(
# MAGIC $"text", $"count"))
# MAGIC .select($"text".as("presidents"), $"count".as("hashtags"))
# MAGIC display(mergedDataFrame)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala
# MAGIC val mergedDataParquet = dataParquePresidentsPuttin
# MAGIC .union(dataFramePresidentsTrump.select(
# MAGIC $"text", $"count"))
# MAGIC .select($"text".as("presidents"), $"count".as("hashtags"))
# MAGIC display(mergedDataParquet)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

########################################################################################################################
####################                                     QUERY 9                                    ####################
####################                                   Description                                  ####################
#################### Show the number of users following a hashtag either related to puttin or trump ####################
########################################################################################################################

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala 
# MAGIC val patternPuttin = ".*putin.*";
# MAGIC val patternTrump = ".*trump.*";
# MAGIC 
# MAGIC val frame = dataFrame
# MAGIC .select($"user.id".as("id"),$"entities.hashtags")
# MAGIC .filter(size($"hashtags") > 0)
# MAGIC .select($"id",explode($"hashtags.text".as("text")).as("text"))
# MAGIC .withColumn("text", regexp_replace(lower(col("text")),patternTrump,"trump"))
# MAGIC .withColumn("text", regexp_replace(lower(col("text")),patternPuttin,"puttin"))
# MAGIC .groupBy($"text",$"id")
# MAGIC .agg(count($"text").as("count"))
# MAGIC .filter($"text" === "trump" || $"text" === "puttin")
# MAGIC 
# MAGIC val trumpFrame = frame.filter($"text" === "trump").select($"text",$"count").agg(count($"count").as("count")).withColumn("president",lit("trump"))
# MAGIC val puttinFrame = frame.filter($"text" === "puttin").select($"text",$"count").agg(count($"count").as("count")).withColumn("president",lit("puttin"))
# MAGIC 
# MAGIC val mergedTwoFrame = trumpFrame
# MAGIC .union(puttinFrame.select(
# MAGIC $"count",$"president"))
# MAGIC .select($"president", $"count".as("users"))
# MAGIC 
# MAGIC display(mergedTwoFrame)

## ------------------------------------------------------ COMMAND ------------------------------------------------------

# MAGIC %scala 
# MAGIC val patternPuttin = ".*putin.*";
# MAGIC val patternTrump = ".*trump.*";
# MAGIC 
# MAGIC val parque = parquetData
# MAGIC .select($"user.id".as("id"),$"entities.hashtags")
# MAGIC .filter(size($"hashtags") > 0)
# MAGIC .select($"id",explode($"hashtags.text".as("text")).as("text"))
# MAGIC .withColumn("text", regexp_replace(lower(col("text")),patternTrump,"trump"))
# MAGIC .withColumn("text", regexp_replace(lower(col("text")),patternPuttin,"puttin"))
# MAGIC .groupBy($"text",$"id")
# MAGIC .agg(count($"text").as("count"))
# MAGIC .filter($"text" === "trump" || $"text" === "puttin")
# MAGIC 
# MAGIC val trumpParque = parque.filter($"text" === "trump").select($"text",$"count").agg(count($"count").as("count")).withColumn("president",lit("trump"))
# MAGIC val puttinParque = parque.filter($"text" === "puttin").select($"text",$"count").agg(count($"count").as("count")).withColumn("president",lit("puttin"))
# MAGIC 
# MAGIC val mergedTwoParque = trumpParque
# MAGIC .union(puttinParque.select(
# MAGIC $"count",$"president"))
# MAGIC .select($"president", $"count".as("users"))
# MAGIC 
# MAGIC display(mergedTwoParque)
